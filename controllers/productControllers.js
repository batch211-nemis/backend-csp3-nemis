const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const auth = require("../auth");



module.exports.addProduct = (data) =>{

	if (data.isAdmin){
		
		return Product.find({name:data.product.name}).then(result=>{

		    if(result.length>0){
		        //let message = "Product name already exists!"
		            return false;

	        } else {
			
		    	let newProduct = new Product({
		        	name : data.product.name,
		        	description : data.product.description,
		        	price : data.product.price,
		        	photo: data.product.photo
		    		})

			    return newProduct.save().then((product,error)=>{
			        if(error){
			            return false;
			        } else {
			            //let message = "PRODUCT CREATED SUCCESSFULLY!";
                    return true;//{message, newProduct};
			        }
			    })
			}
		})

	} else {

		return false; //Promise.resolve("You are not authorized to add a product.");
	}

}


//Retrieve all products
module.exports.getAllProducts = ()=>{
    return Product.find({}).then(result=>{
        return result;
    });
};


//Retrieve all active products
module.exports.getAllActiveProducts = ()=>{
    return Product.find({isActive:true}).then(result=>{
        return result;
    });
};


//Retrieving a single product
module.exports.getProduct = (reqParams)=>{
    return Product.findById(reqParams.productId).then(result=>{
        return result;
    });
};


//Updating a product
module.exports.updateProduct = (reqParams,reqBody)=>{

	let updatedProduct ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	
	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
		
		if(error){
			return false;
		
		}else{
			return true;
		};
	});

};

//Archive a product
module.exports.archiveProduct = (data)=>{

	if (data.isAdmin){

	    let archivedProduct = {isActive: false};

    	return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
	        if(error){
	            return false;
	        } else {
	            return Product.findById(data.productId).then(result=>{return result;});
	        };
	    }); 

    }  else {
    	return false; //Promise.resolve("You are not authorized to archive a product.");
    }
};


//Activate a product
module.exports.activateProduct = (data)=>{

	if (data.isAdmin){

	    let archivedProduct = {isActive: true};

    	return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
	        if(error){
	            return false;
	        } else {
	            return Product.findById(data.productId).then(result=>{return result;});
	        };
	    }); 

    }  else {
    	return false; //Promise.resolve("You are not authorized to activate a product.");
    }
};
