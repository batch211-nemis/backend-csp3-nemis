const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");


//Create New Order
module.exports.checkOut = (data) =>{

    return User.findById(data.userId).then((result) => {

    if (result.isAdmin === true){

        //let message1 = "Unauthorized to create an order.";

        return false;

    } else {

        return Product.findById(data.productId).then((result, err) => {

            let newOrder = new Order({
                userId: data.userId,
                deliveryMode: data.deliveryMode,
                deliveryAddress: data.deliveryAddress,
                totalAmount: data.quantity * result.price,
                products: [{
                    productId: data.productId,
                    quantity: data.quantity,
                    subTotal: data.quantity * result.price
                    }]
                })

                return newOrder.save().then((order,error)=>{

                    if(error){
                        return false;
                    } else {

                    return Order.findOne({userId:data.userId}).then((order, err) => {

                        let pushProduct = {
                          
                            orders: [{
                                orderId: order._id,
                                quantity: data.quantity,
                                subTotal: data.quantity * result.price
                            }]
                        }

                     return Product.findByIdAndUpdate(data.productId, {$push: pushProduct}).then((order,error)=>{

                        //let message = "Order Successful!";

                            if(error){
                                return false;
                            } else {
                                return true;      
                            }
                        })
                     })
                    }
                })
            })
        }
    })
}

//Retrieve All Orders (admin-only)
module.exports.getAllOrders = (data) =>{

    if (data.isAdmin){

        return Order.find({}).then(result=>{return result;});

    } else {

         return false;//Promise.resolve("Unauthorized!");
    }
}


//Retrieve Authenticated User’s Orders
module.exports.getUserOrders = (data) =>{

    return User.findById(data.userId).then((result) => {

        if (result.isAdmin === true){

            //let message1 = "Unauthorized!";

            return false //message1;

        } else {

            return Order.find({userId:data.userId}).then(result=>{return result;});
        }
    })
}

//Retrieving a single order
module.exports.getOrder = (reqParams)=>{
    return Order.findById(reqParams.orderId).then(result=>{
        return result;
    });
};


//Add Orders
module.exports.addOrder = (data) =>{

    // return Order.find({userId:data.userId}).then(result=>{

    //      //let message1 = "You do not have any existing orders, create one first.";

    //     if(result.length == []){
    //         return false //message1;

    //     } else {

             return Product.findById(data.productId).then((result, err) => {

                let addProduct = {
                     products: [{
                         productId:data.productId,
                         quantity: data.quantity,
                         subTotal: data.quantity * result.price
                     }],
                 }

                 return Order.findOneAndUpdate({userId:data.userId}, {$push: addProduct}).then((order,error)=>{

                         if(error){
                             return false;
                         } else {

                            return Order.findOne({userId:data.userId}).then(order=>{

                                let total = {

                                totalAmount: Object.values(order.products).reduce((acc, curr) => (acc = acc + curr["subTotal"]), 0)
                                }
                            
                            return Order.findOneAndUpdate({userId:data.userId}, {$set: total}).then((order,error)=>{

                                    if(error){
                                        return false;
                                    } else {
                                        
                                     return Order.findOne({userId:data.userId}).then((order, err) => {

                                            let pushProduct = {
                                                orders: [{
                                                    orderId: order._id,
                                                    quantity: data.quantity,
                                                    subTotal: data.quantity * result.price
                                                }]
                                            }

                                     return Product.findByIdAndUpdate(data.productId, {$push: pushProduct}).then((order,error)=>{

                                        //let message = "You've added your order successfully!";

                                            if(error){
                                                return false;
                                            } else {
                                                return true; //message;      
                                            }
                                        })
                                     })
                                }
                             }) 
                        })
                    }
                })
            })
        } 
//     })
// }

 
//Retrieve all orders for delivery
module.exports.getDelOrders = (data)=>{

    if (data.isAdmin){

            return Order.find({deliveryMode:"Deliver"}).then(result=>{
            return result});

        } else {

             return false; //Promise.resolve("Unauthorized!");
        }
    }



//Retrieve all orders for meet up
module.exports.getPickOrders = (data)=>{

    if (data.isAdmin){

            return Order.find({deliveryMode:"Pick-up"}).then(result=>{
            return result});

        } else {

             return false //Promise.resolve("Unauthorized!");
        }
    }


