const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Check Email
module.exports.checkEmailExists = (reqBody) =>{
    return User.find({email:reqBody.email}).then(result=>{
        if(result.length>0){
            return true;
        }else{
            return false;
        };
    });
};

//User Registration
module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password,10)
    })

    return newUser.save().then((user,error)=>{
        if(error){
            return false;
        }else{
            return true;
        };
    });
};


//User Authentication
module.exports.loginUser = (reqBody) =>{
    return User.findOne({email:reqBody.email}).then(result=>{
        if(result==null){
            // let message = "Email doesn't exists!";
            return false;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            if(isPasswordCorrect){
                //let message = "WELCOME TO YOUR PROFILE!";
                return { access: auth.createAccessToken(result)}
            } else {
                //let message = "Wrong Password!";
                return false;
            };
        };
    });
};


//Set User as Admin (Admin-only)
module.exports.updateAdmin = (data)=>{

	if (data.isAdmin){

		let updateActiveField = {isAdmin : true};

		return User.findByIdAndUpdate(data.userId, updateActiveField).then((user, error) => {

			if (error) {

				return false;

			} else {

				return User.findById(data.userId).then(result=>{
    
                    return result;
                });

			};

		});

	} else {
		return Promise.resolve("You are not authorized to make user an admin.");
	}
};


module.exports.unSetAdmin = (data)=>{

    if (data.isAdmin){

        let updateActiveField = {isAdmin : false};

        return User.findByIdAndUpdate(data.userId, updateActiveField).then((user, error) => {

            if (error) {

                return false;

            } else {

                return User.findById(data.userId).then(result=>{
    
                    return result;
                });

            };

        });

    } else {
        return Promise.resolve("You are not authorized to make user an admin.");
    }
};

//Retrieve User Details
module.exports.getProfile = (data) =>{
    return User.findById(data.userId).then(result=>{
        result.password = "admin1234";
        return result;
    });
};


//Retrieve all users
module.exports.getAllUsers = ()=>{
    return User.find({}).then(result=>{
        result.password = "";
        return result;
    });
};


module.exports.updateUser = (reqBody)=>{


    let updatedUser ={
        userId: reqBody.userId,
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email:reqBody.email,
        mobileNo:reqBody.mobileNo,
        password:reqBody.password
        
    };
    
    return User.findByIdAndUpdate(reqBody.userId,updatedUser).then((user,error)=>{
        
        if(error){
            return false;
        
        }else{
            return true;
        };
    });

};